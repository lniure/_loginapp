var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressHandlebars = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');

//Routes
var index = require('./routes/index');
var users = require('./routes/users');


//initialise App
var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('hbs',expressHandlebars({defaultLayout:'layout',layoutsDir:__dirname+'/views/layouts/'}));
app.set('view engine', 'hbs');


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));

//BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//express Session
app.use(session({
  secret:'secret',
  resave: true,
  saveUninitialized:true,

}));

//passport init
//app.use(passport.initialize());
//app.use(passport.session());

//Middleware for Validator
// In this example, the formParam value is going to get morphed into form body format useful for printing.
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
    var namespace = param.split('.')
        , root    = namespace.shift()
        , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));
//connect Flash
app.use(flash());

//Global Variables
app.use(function(req,res,next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});


app.use('/', index);
app.use('/users', users);

app.set('port',(process.env.PORT || 3000));
app.listen(app.get('port'),function(){
  console.log('server started on port '+app.get('port'));
});

module.exports = app;
