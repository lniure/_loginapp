var passport = require('passport');
var bcrypt = require('bcryptjs');
var LocalStrategy =require('passport-local').Strategy;
var express = require('express');
var router = express.Router();
var db =require ('../models/user.js');

/* GET users listing. */
router.get('/register', function(req, res, next) {
  res.render('register');
});
router.get('/login', function(req, res, next) {
  res.render('login');
});
router.post('/register', function(req, res, next) {
    var name = req.body.name;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var password2 = req.body.password2;
//validation
    req.checkBody('name','Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('password2', 'Passwords do not match').equals(req.body.password);
    var errors = req.validationErrors();
    if(errors) {
        res.render('register',{
            errors:errors
        });
    }else {
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(req.body.password, salt, function (err, hash) {
                var newUser = {
                    name: name,
                    email: email,
                    username: username,
                    password: hash,
                }
                    db.User.query('INSERT INTO logindata SET ?', newUser, function (err, res) {
                    if (err) throw err;

                    console.log('Last record insert id:', res.insertId);

                });
            });
                });
            }
    req.flash('success_msg','you are registered');
    res.redirect('/users/login');
});

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

module.exports = router;
